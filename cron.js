const express = require('express'),
    fs = require('fs'),
    csv = require('csv-parser'),
    requestHttp = require('request'),
    axios = require('axios'),
    CronJob = require('cron').CronJob,
    app = express();

app.use(express.static('public'));

var file = '',
    success = 0,
    fails = 0,
    failedBarCodes = [];

/**
 * CronJob('segundos, minutos, horas')
 * 
*/
var job = new CronJob('00 00 23 * * 1-5', function () {

        /*
         * Runs every weekday (Monday through Friday)
         * at 00:00:00 PM. It does not run on Saturday
         * or Sunday.
         */

        console.log('Cron inicializada.');
        requestHttp({
            method: 'POST',
            url: 'https://nameless-hamlet-99607.herokuapp.com/item/removeAllItemByPlaceId/',
            form: {place_id: 39}
        })
        .on('response', function(response) {
            console.log(response);
        });

        file = Date.now();

        let logger = fs.createWriteStream('public/files/' + file + '.txt', {
            flags: 'a' // 'a' means appending (old data will be preserved)
        });

        let data = new Date().toLocaleDateString('pt-BR').split('-');
        let dataPtBr = data[2] + '/' + data[1] + '/' + data[0];

        logger.write('Processo iniciado em ' + dataPtBr + ' às ' + new Date().toLocaleTimeString('pt-BR') + '\n');

        /* Creating file CSV on project */
        let ws = fs.createWriteStream('public/files/preco.csv');
        axios({
                method: 'get',
                url: 'http://www.projetogiulia.com.br/tvlar/preco.csv',
                responseType: 'stream'
            })
            .then(function (response) {
                response.data.pipe(ws);
            });

        /* CSV stream options */
        let stream = csv({
            raw: false,
            separator: ';',
            quote: '~',
            newline: '\n',
            headers: ['codigo_barra', 'nome', 'preco', 'quantidade', 'centro_lucro']
        });

        /* After writes the file */
        ws.on('finish', function () {
            var arrayProducts = {
                Produtos: []
            };
            fs.createReadStream('public/files/preco.csv')
                .pipe(stream)
                .on('data', function (data) {
                    arrayProducts.Produtos.push({
                        name: data.nome,
                        descricao: data.nome,
                        preco: data.preco,
                        place_id: 39,
                        codigobarra: data.codigo_barra,
                        centrolucro: data.centro_lucro,
                        quantidade: data.quantidade
                    });
                })
                .on('end', function () {

                    /* Saving all products on database */

                    let saveItems = function (x) {
                        if (x < arrayProducts.Produtos.length) {
                            const params = {
                                name: arrayProducts.Produtos[x].name,
                                descricao: arrayProducts.Produtos[x].descricao,
                                preco: arrayProducts.Produtos[x].preco,
                                place_id: 39,
                                codigobarra: arrayProducts.Produtos[x].codigobarra,
                                centrolucro: arrayProducts.Produtos[x].centrolucro,
                                quantidade: arrayProducts.Produtos[x].quantidade
                            };

                            const options = {
                                method: 'POST',
                                uri: 'https://nameless-hamlet-99607.herokuapp.com/item/UpdateItemOrCreateByBc/',
                                form: params
                            };

                            console.log(`${arrayProducts.Produtos[x].codigobarra} - ${arrayProducts.Produtos[x].quantidade}`);

                            if (params.quantidade != 0) {
                                requestHttp(options)
                                .on('response', function (response) {
                                    if (response.statusCode == 500) {
                                        console.log('Falha ao salvar produto ' + arrayProducts.Produtos[x].codigobarra + ' . Erro: ' + response.statusCode);
                                        fails = fails + 1;
                                        failedBarCodes.push(arrayProducts.Produtos[x].codigobarra);
                                    } else {
                                        response.on('data', function (data) {
                                            console.log('Produto ' + arrayProducts.Produtos[x].codigobarra + ' salvo com sucesso. Recebido: ' + data);
                                            success = success + 1;
                                        });
                                    }
                                    setTimeout(() => {
                                        saveItems(x + 1);
                                        setTimeout(() => {
                                            if (x == arrayProducts.Produtos.length - 1) {
                                                job.stop();
                                            }
                                        }, 250);
                                    }, 250);
                                });
                            } else {
                                setTimeout(() => {
                                    saveItems(x + 1);
                                    setTimeout(() => {
                                        if (x == arrayProducts.Produtos.length - 1) {
                                            job.stop();
                                        }
                                    }, 250);
                                }, 250);
                            }
                        }
                    };
                    saveItems(0);
                });
        });

    }, function () {

        /* This function is executed when the job stops */

        let logger = fs.createWriteStream('public/files/' + file + '.txt', {
            flags: 'a'
        });

        let data = new Date().toLocaleDateString('pt-BR').split('-');
        let dataPtBr = data[2] + '/' + data[1] + '/' + data[0];

        logger.write('\n' + success + ' produtos cadastrados com sucesso.\n');
        logger.write(fails + ' produtos não foram cadastrados.\n');
        logger.write('\nLista dos produtos não cadastrados:\n');
        for (let i = 0; i < failedBarCodes.length; i++) {
            logger.write(failedBarCodes[i] + '\n');
        }
        logger.write('\n');
        logger.write('Processo finalizado em ' + dataPtBr + ' às ' + new Date().toLocaleTimeString('pt-BR'));

        console.log('Cron finalizada.');

    },
    true, /* Start the job right now */
    'America/Manaus' /* Time zone of this job. */
);

const PORT = process.env.PORT || 3000;
app.listen(PORT, '0.0.0.0', () => {
    console.log(`App is running on port ${ PORT }`);
});